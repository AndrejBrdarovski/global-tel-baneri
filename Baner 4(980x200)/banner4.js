function bannerArrow() {
    var sveklase = ['pulsate', 'shake-lr', 'shake-vertical', 'jello-horizontal']
    var random = sveklase[Math.floor(Math.random() * sveklase.length)];

    document.querySelector('.banner-arrow').classList.add('flying-arrow1')
    setTimeout(function() {
        document.querySelector('.banner-arrow').classList.remove('flying-arrow1')
    }, 800);

    document.querySelector('.pink-circle').classList.add(random)
    setTimeout(function() {
        document.querySelector('.pink-circle').classList.remove(random)
    }, 1100);
}

var bannerSlideNo = 0;

function changeBanner() {
    if (bannerSlideNo < 49) {
        if (bannerSlideNo == 48) {
            document.querySelector('.res-btn').classList.add('broken-heart')
        }
        document.querySelector('.question-text').innerHTML = bannerQuestionTexts[bannerSlideNo]['question']
        document.querySelector('.res-text').innerHTML = bannerQuestionTexts[bannerSlideNo]['button']
        if (bannerQuestionTexts[bannerSlideNo]['button1']) {
            document.querySelector('.phone-num').classList.remove('broken-heart')
        } else {
            document.querySelector('.phone-num').classList.add('broken-heart')
        }
        if (bannerQuestionTexts[bannerSlideNo]['hearts']) {
            document.querySelector('.flying-hearts').classList.remove('broken-heart')
        } else {
            document.querySelector('.flying-hearts').classList.add('broken-heart')
        }
        if (bannerQuestionTexts[bannerSlideNo]['lastlink']) {
            document.querySelector('#last-pink-link').classList.remove('broken-heart')
        } else {
            document.querySelector('#last-pink-link').classList.add('broken-heart')
        }
        if (bannerQuestionTexts[bannerSlideNo]['lastlogo']) {
            document.querySelector('#lastlogo').classList.remove('broken-heart')
        } else {
            document.querySelector('#lastlogo').classList.add('broken-heart')
        }
        bannerSlideNo++
    }
}
var bannerQuestionTexts = [{
        question: "Hoćeš da mi daš broj?",
        button: "Neću",
        button1: false,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Verovatno je glup broj.",
        button: "Nije",
        button1: false,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Ali sigurno nije ni dobar.</br>Kao npr:</br><div class='banner-phone-fav'>0677 CAR SAM</div>",
        button: "Pali",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Šaaaaalim se...</br>To baš i ne bi bilo</br>mnogo kul.",
        button: "Ne bi",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Osim ako je tebi kul.",
        button: "Nije",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Meni stvarno ne smeta. Meni si baš</br><div class='banner-phone-fav'>0677 SLATKIŠ</div>",
        button: "Bljak",
        button1: true,
        hearts: true,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Šta te briga šta drugi misle.",
        button: "Briga me",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "'Ajde da probamo još jednom",
        button: "Neću",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "'Aaaaaajde!",
        button: "Neću",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Klikni me opet, znaš da želiš.",
        button: "Ne želim",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Hahahahaha sjajno, hajde sad opet.",
        button: "Neću",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Još jednom...",
        button: "Neću",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Uzdbudljivo, zar ne? Ni meni nije svejedno.",
        button: "Prekini",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Zašto, baš smo kliknuli?",
        button: "Nismo",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "<div class='banner-phone-fav'>0677 KLIKER</div>",
        button: "Užas",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Možda ipak ne... Razmisli još malo?",
        button: "Neću",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Klikni još jednom. Niko ne mora da zna.",
        button: "Klik",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "I meni se to dopada.</br>To što mi imamo.",
        button: "Jao bre",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Što, svima prija malo</br><div class='banner-phone-fav'>0677 FLERTA</div>",
        button: "Bljak",
        button1: true,
        hearts: true,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Ok, probajmo opet.",
        button: "Hajde",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Super ti ide. Može još jednom?",
        button: "Klik",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Nadam se da ne klikćeš tek tako.",
        button: "Klik",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Da za tebe nisam</br>kao i svaki drugi</br>baner</br>klik-klik-klik",
        button: "Klik",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Onda iskuliraš kad te pitam za broj, u fazonu si</br><div class='banner-phone-fav'>0677 PALJBA</div>",
        button: "Tako je",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Ali ipak, još si tu.",
        button: "Aha",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Ništa zanimljivo na TV-u, a?",
        button: "Ništa",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Možda je zanimljivije ovde?",
        button: "Možda",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Meni jeste",
        button: "Pa šta?",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Pa moj broj za tebe bi verovatno bio</br><div class='banner-phone-fav'>0677 NAMYGG</div>",
        button: "Stvarno?",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Ali ne delim brojeve baš tako lako</br><div class='banner-phone-fav'>0677 MOŠICA</div>",
        button: "Pali",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Sad stvarno. Tvoje je samo da osmisliš broj.",
        button: "Zašto?",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Zato što je moje da ti ostvarim želju iako nisi<br><div class='banner-phone-fav'>0677 ALADIN</div>",
        button: "Hahahah",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Nije smešno.</br>Vide li ti duha</br>u novom filmu?",
        button: "Ne",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Blago tebi</br>Aladin je u šoku",
        button: "Njah",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Nego klikni da se vratimo na temu.",
        button: "Klik",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Bravo!</br>Niko kao ti.</br><div class='banner-phone-fav'>0677 THE ONE</div>",
        button: "Pakao",
        button1: true,
        hearts: true,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Šaaaalim se</br><div class='banner-phone-fav'>0677 LEPOTO</div>",
        button: "Bljuc",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Opet se šalim. Iako izgledaš super.",
        button: "Dobro",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Kako želiš da te zovem?",
        button: "Ne želim",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Sigurno imaš nešto na umu, čim si još uvek ovde.",
        button: "Nemam",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Još uvek klikćeš.",
        button: "Klik",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Sa tobom je baš</br><div class='banner-phone-fav'>0677 LAGANO</div>",
        button: "Nije",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Meni je baš divno sve ovo...",
        button: "Uf",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Ma šta divno,</br><div class='banner-phone-fav'>0677 SJAJNO</div>",
        button: "I šta?",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Pa ništa. Ništa što je lepo ne traje večno.",
        button: "Odlično",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Mi bismo možda mogli?",
        button: "Ne bismo",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Ok, deluje da su uzbudljivo i neobavezno tvoj fazon.",
        button: "Možda",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Ja sam <div class='banner-phone-fav'>0677 BANNER</div>,</br>ti smisli svoj broj i probaj uzbudljivi prepaid već od 100rsd.",
        button: "Zašto bih?",
        button1: true,
        hearts: false,
        lastlink: false,
        lastlogo: false
    },
    {
        question: "Zašto da ne?! Uzbudljivo je i ovo je #VezaBezObaveza",
        button: "",
        button1: false,
        hearts: false,
        lastlink: true,
        lastlogo: true
    }
]